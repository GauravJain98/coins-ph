import os

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from django.urls import include, path

BUILD_MODE = os.environ.get("BUILD_STAGE", "LOCAL")

schema_view = get_schema_view(
    openapi.Info(
        title="Coins PH Task",
        default_version="v1",
        description="Coins PH Task created by Gaurav Jain",
        contact=openapi.Contact(email="gauravcdj.98@gmail.com"),
        license=openapi.License(
            name="MIT License", url="https://choosealicense.com/licenses/mit/"
        ),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path("api/", include("main.urls")),
]

if BUILD_MODE not in ["PROD"]:
    from django.contrib import admin

    urlpatterns.append(path("admin/", admin.site.urls))
