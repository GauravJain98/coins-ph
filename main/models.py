from django.db import models


class Account(models.Model):
    name = models.CharField(
        max_length=32,
        null=False,
        unique=True,
        primary_key=True,
        help_text="unique name and id of the account",
    )
    balance = models.PositiveIntegerField(help_text="balance of the account")

    def __str__(self):
        return f"{self.name}"


class Transaction(models.Model):
    from_account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name="from_transactions",
    )
    to_account = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name="to_transactions"
    )
    amount = models.IntegerField(
        help_text="amount to be transfered from_account to to_account",
    )

    def __str__(self):
        return f"{self.from_account} {self.amount} {self.to_account}"
