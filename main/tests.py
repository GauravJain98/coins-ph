from django.test import Client, TestCase
from main.models import Account


class TransactionTestCase(TestCase):
    def setUp(self):
        self.accounts = [
            {
                "name": "bob",
                "balance": 40000,
            },
            {
                "name": "susan",
                "balance": 40000,
            },
        ]
        for account in self.accounts:
            Account.objects.create(**account)

    def test_transaction(self):
        """Transaction check"""
        from_account = self.accounts[0]
        to_account = self.accounts[1]
        amount = 500
        c = Client(enforce_csrf_checks=True)
        data = {
            "from_account": from_account["name"],
            "to_account": to_account["name"],
            "amount": amount,
        }

        res = c.post("/api/transactions/", data=data)
        self.assertDictEqual(res.json(), data)
        from_account_obj = Account.objects.get(name=from_account["name"])
        to_account_obj = Account.objects.get(name=to_account["name"])
        self.assertEqual(to_account_obj.balance, to_account["balance"] + amount)
        self.assertEqual(from_account_obj.balance, from_account["balance"] - amount)
