from rest_framework.routers import DefaultRouter

from django.urls import include, path
from main.views import AccountViewSet, TransactionViewSet

router = DefaultRouter()
router.register(r"transactions", TransactionViewSet, basename="transactions")
router.register(r"accounts", AccountViewSet, basename="accounts")

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path("", include(router.urls)),
]
