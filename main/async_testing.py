import asyncio

import aiohttp
import requests

url = "http://hidden-chamber-71834.herokuapp.com"


async def transaction_async():
    """Transaction check"""

    async def run_request(data, i):
        print(f"i: {i} started")
        async with aiohttp.ClientSession() as session:

            async with session.post(f"{url}/api/transactions/", data=data) as resp:
                res = await resp.json()
                print(res)

    data = {
        "from_account": "bob",
        "to_account": "susan",
        "amount": 500,
    }
    tasks = list()
    for i in range(5):
        tasks.append(asyncio.create_task(run_request(data, i)))
    await asyncio.wait(tasks)


data = [
    {
        "name": "susan",
        "balance": 5000,
    },
    {
        "name": "bob",
        "balance": 5000,
    },
]


requests.post(f"{url}/api/accounts/", data=data[0])
requests.post(f"{url}/api/accounts/", data=data[1])
task = asyncio.run(transaction_async())
