from django_filters import rest_framework as filters
from rest_framework import mixins, status, viewsets
from rest_framework.response import Response

from django.db import transaction
from django.db.models import F
from main.models import Account, Transaction
from main.serializers import AccountSerializer, TransactionSerializer


class TransactionViewSet(
    mixins.CreateModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet
):
    """
    Viewset for transactions
    """

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ("from_account", "to_account")

    @transaction.atomic()
    def create(self, request):
        """
        Transfer payment from one account to another
        """
        add_transaction_body = self.serializer_class(data=request.data)

        # validate transaction_body
        if not add_transaction_body.is_valid():
            return Response(
                add_transaction_body.errors, status=status.HTTP_400_BAD_REQUEST
            )

        from_account = add_transaction_body.validated_data["from_account"]
        to_account = add_transaction_body.validated_data["to_account"]
        amount = add_transaction_body.validated_data["amount"]

        from_account_obj = Account.objects.select_for_update().filter(
            name=from_account, balance__gte=amount
        )  # Make sure account has enough funds and lock row

        if not from_account_obj.exists():
            return Response(
                {"message": f"{from_account} doesnt have enough funds to transfer"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # Update balances
        to_account_obj = Account.objects.select_for_update().filter(name=to_account)
        from_account_obj.update(
            balance=F("balance") - amount
        )  # F query allows the updation be
        to_account_obj.update(balance=F("balance") + amount)

        # Add Transaction
        add_transaction_body.save()
        return Response(add_transaction_body.data)


class AccountViewSet(
    mixins.ListModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet
):
    """
    list:
    Return a list of all the existing accounts.

    create:
    Create a new account.
    """

    queryset = Account.objects.all()
    serializer_class = AccountSerializer
