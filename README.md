# CoinsPH Task

Task for coins.ph backend task created by [Gaurav Jain](https://github.com/GauravJain98/)

## Install Dependencies

Use the package manager [pip](https://pip.pypa.io/en/stable/) or you can also setup this project using [poetry](https://python-poetry.org/)

### Install using venv

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt # Install dependencies
```

### Install using poetry

```bash
poetry install # Install dependencies
poetry shell
```

## Run Development server

```bash
python3 manage.py collectstatic --noinput
python3 manage.py migrate # Add migration to local db
python3 manage.py runserver # Run local server at :8000
```

## Run the tests

### Run simple tests

```bash
python3 manage.py test
```

### Run race condition tests

```bash
python3 manage.py runserver
python3 main/async_testing.py
```

## Deployment

The project is deployed on [Heroku](https://www.heroku.com/) using Docker on [https://hidden-chamber-71834.herokuapp.com/api](https://hidden-chamber-71834.herokuapp.com/api)

## Documentation

The project is documented using [OpenAPI Swagger](https://swagger.io/specification/) using [drf-yasg](https://drf-yasg.readthedocs.io/en/stable/)

URL: [https://hidden-chamber-71834.herokuapp.com/swagger/](https://hidden-chamber-71834.herokuapp.com/swagger/)

## License

[MIT](https://choosealicense.com/licenses/mit/)
